import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { interval, Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

@Component
({
  selector: 'app-live-list',
  templateUrl: './live-list.component.html',
  styleUrls: ['./live-list.component.css']
})
export class LiveListComponent implements OnInit
{
  @Input('url') url:string;


  poller:Observable<number> = interval(3000);

  constructor(private http:HttpClient) { }

  ngOnInit()
  {
  }


  startPolling()
  {

  }

  stopPolling()
  {

  }


}
