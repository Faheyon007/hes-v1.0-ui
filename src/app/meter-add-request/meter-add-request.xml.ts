

export let requestMeterAdd = `
  <?xml version="1.0" encoding="UTF-8"?>
  <RequestMessage xmlns="http://iec.ch/TC57/2011/schema/message"
          xmlns:m="http://iec.ch/TC57/2011/MeterConfig#"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://iec.ch/TC57/2011/schema/message Message.xsd">

    <Header>
      <Verb>create</Verb>
      <Noun>MeterConfig</Noun>
      <Timestamp>2018-02-06 17:01:54</Timestamp>
      <Source>MDM</Source>
      <AsyncReplyFlag>false</AsyncReplyFlag>
      <ReplyAddress></ReplyAddress>
      <AckRequired>false</AckRequired>
      <MessageID>36c1fe98-bacb-42a1-8716-1216ce2be76c</MessageID>
      <CorrelationID>R1517914914150</CorrelationID>
      <Revision>2.0</Revision>
    </Header>

    <Payload>
      <m:MeterConfig>
        <m:Meter>
          <m:mRID>METER_ID</m:mRID>
          <m:Names>
            <m:name>541790023960</m:name>
            <m:NameType>
              <m:name>serialNumber</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>CL710</m:name>
            <m:NameType>
              <m:name>meterModel</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>02</m:name>
            <m:NameType>
              <m:name>meterMode</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>04</m:name>
            <m:NameType>
              <m:name>meterType</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>54</m:name>
            <m:NameType>
              <m:name>manufacturer</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>1/1</m:name>
            <m:NameType>
              <m:name>ctRatio</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>1/1</m:name>
            <m:NameType>
              <m:name>ptRatio</m:name>
            </m:NameType>
          </m:Names>

          <m:Names>
            <m:name>03</m:name>
            <m:NameType>
              <m:name>protocolType</m:name>
            </m:NameType>
          </m:Names>
        </m:Meter>
      </m:MeterConfig>
    </Payload>
  </RequestMessage>  
  `;