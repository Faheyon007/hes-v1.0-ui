// import { Component, OnInit } from '@angular/core';

// import { HttpClient } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http';

// import { requestMeterAdd } from './meter-add-request.xml'
// import { SingleInputFormComponent } from '../components/single-input-form/single-input-form.component';


// @Component({
//   selector: 'app-meter-add-request',
//   templateUrl: './meter-add-request.component.html',
//   styleUrls: ['./meter-add-request.component.css']
// })
// export class MeterAddRequestComponent extends SingleInputFormComponent
// {
//   txtPlaceHolder = "Enter Meter ID";
//   txtSubmit = "Add";



//   url = "http://192.168.11.53:90/HesWebService.asmx?op=doCommand";


//   meterId = "";


//   httpHeaderOptions =
//   { 
//     headers: new HttpHeaders
//       ({
//         'Content-Type': 'text/xml;charset="utf-8"',
//         'Accept': 'text.xml'
//       })
//   };


//   constructor(protected http: HttpClient) { super(http); }


//   onSubmit()
//   {
//     console.log("Submitting...");

//     let request = this.getMeterAddRequestXML();
//     console.log("Request: " + request);

//     this.http.post(this.url, request, this.httpHeaderOptions).subscribe((response:String) => {
//       console.log("Response: " + response);

//       response.search('<Result>OK</Result>') == -1 ? this.onSubmitFailure() : this.onSubmitSuccess();
//     });

//   }


//   getMeterAddRequestXML()
//   {
//     return requestMeterAdd.replace("METER_ID", this.meterId);
//   }

//   onSubmitSuccess()
//   {
//     // Popup success msg
//     // Add meter to meter-list
//   }

//   onSubmitFailure()
//   {
//     // Popup failure msg
//   }
// }
