export const placeholderSoapBody = "SOAP_BODY_PLACEHOLDER";


export let soapEnvelope = `
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope 
	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    
    <soap:Body>
        
        <doCommand xmlns="http://ClouHesService.com/" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
			<command>${placeholderSoapBody}</command>
        </doCommand>
            
    </soap:Body>
</soap:Envelope>
`