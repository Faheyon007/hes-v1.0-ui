export enum RequestType
{
    ADD_METER,
    ADD_DCU,
    LINK,
    UNLINK,
    SEND_TOKEN,
    REQ_DATA
}