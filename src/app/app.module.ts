import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SingleInputFormComponent } from './components/single-input-form/single-input-form.component';
import { ListComponent } from './list/list.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LiveListComponent } from './live-list/live-list.component';
import { AddableListComponent } from './addable-list/addable-list.component';
import { LiveTabComponent } from './live-tab/live-tab.component';



@NgModule({
  declarations: [
    AppComponent,
    SingleInputFormComponent,
    ListComponent,
    HeaderComponent,
    FooterComponent,
    LiveListComponent,
    AddableListComponent,
    LiveTabComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
