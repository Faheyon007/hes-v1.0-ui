//////////////////////////////////////////////////////////////////////////////////////
//                                  PLACEHOLDERS
//////////////////////////////////////////////////////////////////////////////////////

export let placeholderMeterId = 'METER_ID_PLACEHOLDER';
export let placeholderDcuId = 'DCU_ID_PLACEHOLDER';
export let placeholderToken = 'TOKEN_PLACEHOLDER';

//////////////////////////////////////////////////////////////////////////////////////
//                                  METER ADD
//////////////////////////////////////////////////////////////////////////////////////

export let requestMeterAdd = `
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://iec.ch/TC57/2011/schema/message"
        xmlns:m="http://iec.ch/TC57/2011/MeterConfig#"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://iec.ch/TC57/2011/schema/message Message.xsd">

  <Header>
    <Verb>create</Verb>
    <Noun>MeterConfig</Noun>
    <Timestamp>2018-02-06 17:01:54</Timestamp>
    <Source>MDM</Source>
    <AsyncReplyFlag>false</AsyncReplyFlag>
    <ReplyAddress></ReplyAddress>
    <AckRequired>false</AckRequired>
    <MessageID>36c1fe98-bacb-42a1-8716-1216ce2be76c</MessageID>
    <CorrelationID>R1517914914150</CorrelationID>
    <Revision>2.0</Revision>
  </Header>

  <Payload>
    <m:MeterConfig>
      <m:Meter>
        <m:mRID>${placeholderMeterId}</m:mRID>
        <m:Names>
          <m:name>541790023960</m:name>
          <m:NameType>
            <m:name>serialNumber</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>CL710</m:name>
          <m:NameType>
            <m:name>meterModel</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>02</m:name>
          <m:NameType>
            <m:name>meterMode</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>04</m:name>
          <m:NameType>
            <m:name>meterType</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>54</m:name>
          <m:NameType>
            <m:name>manufacturer</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>1/1</m:name>
          <m:NameType>
            <m:name>ctRatio</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>1/1</m:name>
          <m:NameType>
            <m:name>ptRatio</m:name>
          </m:NameType>
        </m:Names>

        <m:Names>
          <m:name>03</m:name>
          <m:NameType>
            <m:name>protocolType</m:name>
          </m:NameType>
        </m:Names>
      </m:Meter>
    </m:MeterConfig>
  </Payload>
</RequestMessage>  
`;

//////////////////////////////////////////////////////////////////////////////////////
//                                  DCU ADD
//////////////////////////////////////////////////////////////////////////////////////

export let requestDcuAdd = `
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://iec.ch/TC57/2011/schema/message" 
				xmlns:m="http://iec.ch/TC57/2011/EndDeviceConfig#" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:schemaLocation="http://iec.ch/TC57/2011/schema/message Message.xsd">
				
	<Header>
		<Verb>create</Verb>
		<Noun>EndDeviceConfig</Noun>
		<Timestamp>2018-02-06 17:07:28</Timestamp>
		<Source>MDM</Source>
		<AsyncReplyFlag>false</AsyncReplyFlag>
		<ReplyAddress>http://202.51.191.198/prepay/services/CimMDM?wsdl</ReplyAddress>
		<AckRequired>false</AckRequired>
		<MessageID>1502ad79-8473-4d43-9309-180c6ca28cca</MessageID>
		<CorrelationID>R1517915248490</CorrelationID>
		<Revision>2.0</Revision>
	</Header>
	
	<Payload>
		<m:EndDeviceConfig>
			<m:EndDevice>
				<m:mRID>${placeholderDcuId}</m:mRID>
					<m:Names>
						<m:name>201706000004</m:name>
						<m:NameType>
							<m:name>serialNumber</m:name>
						</m:NameType>
					</m:Names>
					
					<m:Names>
						<m:name>16</m:name>
						<m:NameType>
							<m:name>protocolType</m:name>
						</m:NameType>
					</m:Names>
			</m:EndDevice>
		</m:EndDeviceConfig>
	</Payload>
</RequestMessage>
`;

//////////////////////////////////////////////////////////////////////////////////////
//                                  SEND TOKEN
//////////////////////////////////////////////////////////////////////////////////////

export let requestSendToken = `
<?xml version="1.0" encoding="UTF-8"?>
<RequestMessage xmlns="http://iec.ch/TC57/2011/schema/message"
				xmlns:m="http://iec.ch/TC57/2011/EndDeviceControls#"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xsi:schemaLocation="http://iec.ch/TC57/2011/schema/message Message.xsd">

  <Header>
    <Verb>create</Verb>
    <Noun>EndDeviceControls</Noun>
    <Timestamp>2018-02-06 17:26:15</Timestamp>
    <Source>MDM</Source>
    <AsyncReplyFlag>true</AsyncReplyFlag>
    <ReplyAddress>http://202.51.191.198/prepay/services/CimMDM?wsdl</ReplyAddress>
    <AckRequired>true</AckRequired>
    <MessageID>R1517916375494</MessageID>
    <CorrelationID>R1517916375494</CorrelationID>
    <Revision>2.0</Revision>
  </Header>
  <Payload>
    <m:EndDeviceControls>
      <m:EndDeviceControl>
        <m:reason>sendTokens</m:reason>
        <m:EndDeviceControlType ref="15.13.112.78"/>
        <m:EndDevices>
          <m:mRID>${placeholderToken}</m:mRID>
          <m:Names>
            <m:name>73774090485911300197,55214049314869667248,46094296236715987179,52586766326408661816,00425687485334449836,50769734193650914659,62040606495746712139,20338219031249721869</m:name>
          </m:Names>
        </m:EndDevices>
      </m:EndDeviceControl>
    </m:EndDeviceControls>
  </Payload>
</RequestMessage>
`;