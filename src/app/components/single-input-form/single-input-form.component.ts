import { Component, Input, Output, EventEmitter } from "@angular/core";
import { RequestType } from "../../request-type.enum";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
    selector: 'single-input-form',
    templateUrl: './single-input-form.component.html',
    styleUrls: []
})
export class SingleInputFormComponent
{
    @Input('type') requestType:RequestType;
    @Input('placeholder') txtPlaceholder = "";
    @Input('submit') txtSubmit = "";
    @Input('url') url = "";
    @Input('value') value = "";
    @Input('validate') responseValidation(response:string) {}
    @Input('headerOptions') headerOptions:{headers:HttpHeaders}
    @Input('requestBody') getRequestBody;

    @Output('eventSuccess') eventSuccess = new EventEmitter();


    constructor(private http:HttpClient)
    {}


    onSubmit()
    {
        let body = this.getRequestBody([this.value]);
        console.log("[ Request ]:\n" + body + "\n\n");

        this.http.post(this.url, body, this.headerOptions).subscribe((response) => {
            console.log("[ Response ]:\n" + response as string + "\n\n");
            this.responseValidation(response as string) ? this.onSuccess(): this.onFail();
        });
    }

    onSuccess()
    {
        this.eventSuccess.emit({ type:this.requestType, value:this.value });
    }

    onFail()
    {

    }
}