import { Component, Input, Output, EventEmitter } from "@angular/core";
import { RequestType } from "../../request-type.enum";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SingleInputFormComponent } from '../single-input-form/single-input-form.component';



export interface DualInputFormParams
{
    first:SingleInputFormComponent;
    second:SingleInputFormComponent;
}


@Component({
    selector: 'dual-input-form',
    templateUrl: './dual-input-form.component.html',
    styleUrls: []
})
export class DualInputFormComponent
{
    @Input('type') requestType:RequestType;
    @Input('placeholder') txtPlaceholder = "";
    @Input('submit') txtSubmit = "";
    @Input('url') url = "";
    @Input('value') value = "";
    @Input('validate') responseValidation(response:string) {}
    @Input('headerOptions') headerOptions:{headers:HttpHeaders}
    @Input('requestBody') getRequestBody;

    @Output('eventSuccess') eventSuccess = new EventEmitter();


    @Input('meterId') meterIdForm:SingleInputFormComponent;
    @Input('dcuId') dcuIdForm:SingleInputFormComponent;


    constructor(private http:HttpClient)
    {}


    onSubmit()
    {
        let body = this.getRequestBody([this.value]);
        console.log("[ Request ]:\n" + body + "\n\n");

        this.http.post(this.url, body, this.headerOptions).subscribe((response) => {
            console.log("[ Response ]:\n" + response as string + "\n\n");
            this.responseValidation(response as string) ? this.onSuccess(): this.onFail();
        });
    }

    onSuccess()
    {
        this.eventSuccess.emit({ type:this.requestType, value:this.value });
    }

    onFail()
    {

    }
}