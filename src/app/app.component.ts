import { Component } from '@angular/core';
import { RequestType } from './request-type.enum';
import * as requests from './requests.xml';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as soap from '../app/soap.xml';
import * as Socket from '../assets/js/socket.io';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
  socket = Socket();
  
  
  
  constructor(private http:HttpClient){
    // meter add
    this.socket.on('meter-add-response', (json) => {
      console.log(json);
      
      if(json['status'] == 'ok')
      {
        this.meterConfig.items.push(json['meter-id']);
      }
    });

    // dcu add
    this.socket.on('dcu-add-response', (json) => {
      console.log(json);
      if(json['status'] == 'ok')
      {
        this.dcuConfig.items.push(json['dcu-id']);
      }
    });

    // link
    this.socket.on('link-response', (json) => {
      console.log(json);
      if(json['status'] == 'ok')
      {
        if(!this.linkedDcuMap[json['meter-id']])
        {
          this.linkedDcuMap[json['meter-id']] = json['dcu-id'];
        }
      }
    });

    // unlink
    this.socket.on('unlink-response', (json) => {
      console.log(json);
      if(json['status'] == 'ok')
      {
        delete this.linkedDcuMap[json['meter-id']];
      }
    });

    // token
    this.socket.on('token-send-response', (json) => {
      console.log(json);
      if(json['status'] == 'ok')
      {
        console.log('Token successfully sent for Meter-Id: '+ json['meter-id']);
        this.tokenSendSuccess = true;
        console.log('Before tokenSendSuccess: ' + this.tokenSendSuccess);
        setTimeout( () => { this.tokenSendSuccess = false; console.log('In tokenSendSuccess: ' + this.tokenSendSuccess); this.token = ''; }, 2000);
      }
    });
  }




  title = 'Donsun MDMS';

  listTabTitle = 'list';
  liveTabTitle = 'live';
  activeTab = this.listTabTitle;


  url = 'http://119.148.42.42:90/HesWebService.asmx?op=doCommand';



  meterList = {label:'Meter List', items:['A', 'B', 'C']};
  dcuList = {label:'DCU List', items:['AA', 'BB', 'CC']};

  lists = [ this.meterList, this.dcuList ];


  httpHeaderOptions =
  {
    headers: new HttpHeaders
      ({
        'Content-Type': 'text/xml',
        'Accept': 'text/xml'
      })
  };

  meterAddForm =
  {
    type: RequestType.ADD_METER,
    placeholder: 'Enter Meter ID',
    submit: 'Add',
    url: 'http://119.148.42.42:90/HesWebService.asmx?op=doCommand',
    value: '',
    headerOptions: this.httpHeaderOptions,
    validate: (response:string) => { console.log('Meter Add Request'); return true; },
    requestBody: (meterId:string) => { return this.getAddMeterRequestXML(meterId); }
  };

  dcuAddForm =
  {
    type: RequestType.ADD_DCU,
    placeholder: 'Enter DCU ID',
    submit: 'Add',
    url: 'http://119.148.42.42:90/HesWebService.asmx?op=doCommand',
    value: '',
    headerOptions: this.httpHeaderOptions,
    validate: (response:string) => { console.log('DCU Add Request'); return true; },
    requestBody: (dcuId: string) => { return this.getAddDcuRequestXML(dcuId); }
  };

  sendTokenFormParams =
  {
    type: RequestType.SEND_TOKEN,
    placeholder: 'Enter Token',
    submit: 'Send',
    url: 'http://119.148.42.42:90/HesWebService.asmx?op=doCommand',
    value: '',
    headerOptions: this.httpHeaderOptions,
    validate: (response:string) => { console.log('Send Token Request'); return true; },
    requestBody: (meterId:string, token: string) => { return this.getSendTokenRequestXML(meterId, token); }
  };

  forms = [ this.meterAddForm, this.dcuAddForm, this.sendTokenFormParams ];



  onAddSuccess(item:{type:RequestType, value:string})
  {
    console.log('Added Successfully.');

    switch(item.type)
    {
      case RequestType.ADD_METER : this.meterList.items.push(item.value); break;
      case RequestType.ADD_DCU: this.dcuList.items.push(item.value); break;
    }
  }

  clearAllLists()
  {
    for(let list of this.lists)
    {
      list.items = [];
    }
  }


  getAddMeterRequestXML(meterId:string)
  {
    return requests.requestMeterAdd.replace(requests.placeholderMeterId, meterId);
  }

  getAddDcuRequestXML(dcuId:string)
  {
    return requests.requestDcuAdd.replace(requests.placeholderDcuId, dcuId);
  }

  getSendTokenRequestXML(meterId:string, token:string)
  {
    return requests.requestSendToken.replace(requests.placeholderToken, token);
  }

  getLinkRequestXML(meterId:string, dcuId:string)
  {
    return requests.requestDcuAdd.replace(requests.placeholderDcuId, dcuId).replace(requests.placeholderMeterId, meterId);
  }

  getDataRequestXML(id:string)
  {
    
  }




  /////////////////////////////////////////////////////////////////////////////////////////

  selectedMeterId = '';
  selectedDcuId = '';
  token = '';
  tokenSendSuccess = false;;

  linkedDcuMap = {};


  meterConfig = 
  {
    requestType: RequestType.ADD_METER,
    label: 'Meter List',
    items: [],
    placeholder: 'Enter Meter ID',
    onSelectedItemChanged: this.onSelectedItemChanged
  };

  dcuConfig =
  {
    requestType: RequestType.ADD_DCU,
    label: 'DCU List',
    items: [],
    placeholder: 'Enter DCU ID',
    onSelectedItemChanged: this.onSelectedItemChanged
  };

  listConfigs = [
    // {
    //   requestType: RequestType.ADD_METER,
    //   label: 'Meter List',
    //   items: [ 1, 2, 3, 4, 5],
    //   placeholder: 'Enter Meter ID',
    //   onSelectedItemChanged: this.onSelectedItemChanged
    // },
    // {
    //   requestType: RequestType.ADD_DCU,
    //   label: 'DCU List',
    //   items: [ 11, 22 ],
    //   placeholder: 'Enter DCU ID',
    //   onSelectedItemChanged: this.onSelectedItemChanged
    // }
    this.meterConfig,
    this.dcuConfig
  ]


  isLinkable()
  {
    return this.selectedMeterId && this.selectedDcuId && !this.linkedDcuMap[this.selectedMeterId] ? true : false;
  }

  link()
  {
    return this.onRequest({requestType:RequestType.LINK, value:[this.selectedMeterId, this.selectedDcuId]});
  }

  unlink()
  {
    //if success
    this.onRequest({requestType:RequestType.UNLINK, value:[this.selectedMeterId, this.selectedDcuId]});
  }

  isUnlinkable()
  {
    return this.linkedDcuMap[this.selectedMeterId];
  }


  sendToken()
  {
    this.onRequest({requestType:RequestType.SEND_TOKEN, value:[this.selectedMeterId, this.token]});
  }

  isTokenSendable()
  {
    return this.selectedMeterId && this.token ? true : false;
  }

  selectedItems()
  {  
    console.log('Selected Meter Id: ' + this.selectedMeterId);
    console.log('Selected DCU Id: ' + this.selectedDcuId);
  }

  onSelectedItemChanged(eventArgs)
  {
    switch(eventArgs.label)
    {
      case 'Meter List': this.selectedMeterId = eventArgs.item; break;
      case 'DCU List': this.selectedDcuId = eventArgs.item; break;
    }

    this.selectedItems();
  }

  onRequest(eventArgs)
  {
    console.log('[ onRequest ]:' + '\nRequestType: ' + eventArgs.requestType + '\nValue: ' + eventArgs.value);
    
    switch(eventArgs.requestType)
    {
      case RequestType.ADD_METER: this.requestAddMeter(eventArgs.value); break;
      case RequestType.ADD_DCU: this.requestAddDcu(eventArgs.value); break;
      case RequestType.SEND_TOKEN: this.requestSendToken(eventArgs.value[0],eventArgs.value[1]); break;
      case RequestType.LINK: this.requestLink(eventArgs.value[0],eventArgs.value[1]); break;
      case RequestType.UNLINK: this.requestUnLink(eventArgs.value[0],eventArgs.value[1]); break;
      // case RequestType.REQ_DATA: this.requestData(eventArgs.value); break;
    }
  }




  requestAddMeter(meterId)
  {
    this.socket.emit('meter-add-request', { 'meter-id': meterId});

    // console.log('\n[ requestAddMeter ]\n');

    // let xml = this.createSoapRequest(this.getAddMeterRequestXML(meterId));
    // console.log('\n\n[ Request: ]\n' + xml);
    // this.http.post(this.url, xml, this.httpHeaderOptions).subscribe((response) => {
    //     console.log('\n\nResponse:\n' + this.getSoapResponse(response));
    //     this.meterConfig.items.push(meterId);
    // }, (err) => {
    //   console.log('Error: ' + err);
    // });

  }

  requestAddDcu(dcuId)
  {
    this.socket.emit('dcu-add-request', { 'dcu-id': dcuId});

    // console.log('\n[ requestAddDcu ]\n');

    // let xml = this.createSoapRequest(this.getAddDcuRequestXML(dcuId));
    // console.log('\nrequest:\n' + xml);
    // this.http.post(this.url, xml, this.httpHeaderOptions).subscribe((response) => {
    //   console.log('\n\nResponse:\n' + this.getSoapResponse(response));
    //     this.dcuConfig.items.push(dcuId);
    // }, (err) => {
    //   console.log('Error: ' + err);
    // });
  }

  requestSendToken(meterId, token)
  {
    this.socket.emit('token-send-request', { 'meter-id': meterId, 'token':token});

    // console.log('\n[ requestSendToken ]\n');

    // let xml = this.createSoapRequest(this.getSendTokenRequestXML(meterId, token));
    // console.log('\nrequest:\n' + xml);
    // this.http.post(this.url, xml, this.httpHeaderOptions).subscribe((response) => {
    //   console.log('\n\nResponse:\n' + this.getSoapResponse(response));
    // }, (err) => {
    //   console.log('Error: ' + err);
    // });
    
  }

  requestLink(meterId, dcuId)
  {
    this.socket.emit('link-request', { 'meter-id': meterId, 'dcu-id':dcuId});

    // console.log('\n[ requestLink ]\n');

    // let xml = this.createSoapRequest(this.getLinkRequestXML(meterId, dcuId));
    // console.log('\nrequest:\n' + xml);
    // this.http.post(this.url, xml, this.httpHeaderOptions).subscribe((response) => {
    //   console.log('\n\nResponse:\n' + this.getSoapResponse(response));
    //   if(!this.linkedDcuMap[this.selectedMeterId])
    //   {
    //     this.linkedDcuMap[this.selectedMeterId] = this.selectedDcuId;
    //   }
    // }, (err) => {
    //   console.log('Error: ' + err);
    // });

  }

  requestUnLink(meterId, dcuId)
  {
    this.socket.emit('unlink-request', { 'meter-id': meterId, 'dcu-id':dcuId});

    // console.log('\n[ requestLink ]\n');

    // let xml = this.createSoapRequest(this.getLinkRequestXML(meterId, dcuId));
    // console.log('\nrequest:\n' + xml);
    // this.http.post(this.url, xml, this.httpHeaderOptions).subscribe((response) => {
    //   console.log('\n\nResponse:\n' + this.getSoapResponse(response));
    //   if(!this.linkedDcuMap[this.selectedMeterId])
    //   {
    //     this.linkedDcuMap[this.selectedMeterId] = this.selectedDcuId;
    //   }
    // }, (err) => {
    //   console.log('Error: ' + err);
    // });

  }




  // escapeXML(xml:string)
  // {
  //   return xml.replace(/&/g, '&amp;')
  //             .replace(/</g, '&lt;')
  //             .replace(/>/g, '&gt;')
  //             .replace(/\s{2}/g, '')
  //             .replace(/[\n\t]/g, '');
  // }

  // unescapeXML(escapedXml:string)
  // {
  //   return escapedXml .replace(/&apos;/g, "'")
  //                     .replace(/&quot;/g, '"')
  //                     .replace(/&gt;/g, '>')
  //                     .replace(/&lt;/g, '<')
  //                     .replace(/&amp;/g, '&');
  // }


  // createSoapRequest(body)
  // {
  //   return soap.soapEnvelope.replace(soap.placeholderSoapBody, this.escapeXML(body));
  // }

  // getSoapResponse(soap)
  // {
  //   return this.unescapeXML(soap.match(/\&lt\;\?xml.*\&gt\;/g).length == 1 ? soap.match(/\&lt\;\?xml.*\&gt\;/g)[0] : 'No/Multiple match found!');
  // }


}
