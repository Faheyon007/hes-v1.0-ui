import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestType } from '../request-type.enum';

@Component({
  selector: 'app-addable-list',
  templateUrl: './addable-list.component.html',
  styleUrls: ['./addable-list.component.css']
})
export class AddableListComponent implements OnInit
{

  @Input('requestType') requestType:RequestType;
  @Input('label') label:string;
  @Input('items') items:string[];
  @Input('placeholder') placeholder:string;
  @Input('linkedDcuMap') linkedDcuMap:{};
  @Output('selectedItemChanged') selectedItemChanged = new EventEmitter();
  @Output('itemAddEvent') itemAddEvent = new EventEmitter();
  
  selectedItem = '';
  

  input = '';


  constructor() { }

  ngOnInit()
  {
  }



  onAdd()
  {
    // this.items.push(this.input);
    this.itemAddEvent.emit({requestType:this.requestType, value:this.input});
    this.input = '';
  }

  onRemoveAll()
  {
    this.items.length = 0;
    this.onSelectedItemChanged('');
  }

  getSelectedItem()
  {
    return this.selectedItem;
  }

  onSelectedItemChanged(item)
  {
    this.selectedItem = item;
    this.selectedItemChanged.emit({label:this.label, item:this.selectedItem});
  }

  isLinked(item)
  {
    return this.linkedDcuMap[item];
  }

}
