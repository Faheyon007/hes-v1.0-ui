import { Component, OnInit } from '@angular/core';
import * as Socket from '../../assets/js/socket.io';

@Component({
  selector: 'app-live-tab',
  templateUrl: './live-tab.component.html',
  styleUrls: ['./live-tab.component.css']
})
export class LiveTabComponent implements OnInit {

  socket = Socket();

  types = [ 'voltage', 'current', 'frequency', 'energy', 'power' ];

  data = 
  {
    voltage: '210 Hz',
    current: '1.8 Amp',
    frequency: '47 Hz',
    energy: '250 J',
    power: '1.8 KWh'
  };


  meters = {};

  keys = [];



  constructor() {
    this.socket.on('meter-data-response', (json) => {
      // console.log('JSON: \n' + json);
      
      // this.meters[json['meter-id']] = json['data'];
      // console.log(this.meters);

      // this.keys = Object.keys(this.meters);
      
      this.onReceiveData(json);

    });
   }

  ngOnInit() {
    
  }

  
  requestData(meterId)
  {
    this.socket.emit('meter-data-request', {'meter-id':meterId});
  }
  
  onReceiveData(json)
  {
    this.data = json['data'];
    this.meters[json['meter-id']] = this.data;
    console.log(this.meters);
  
    this.keys = Object.keys(this.meters);
  }

}
